# NintenTools.Yaz0

This is a .NET library providing support for decompressing Nintendo Yaz0 data.

The library is available as a [NuGet package](https://www.nuget.org/packages/Syroot.NintenTools.Yaz0).

## Deprecation Notice

**This project aswell as other NintenTools projects is no longer updated or maintained.**
The following known issues result from this:
- Compression support was never added, this library can only decompress data.
